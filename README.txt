.-..----.  .----. .-. . .-..-. .-. .---.    .----. .----.  .--.  .----. .-.   .-..----.
| || {}  }/  {}  \| |/ \| ||  `| |/   __}   | {}  }| {_   / {} \ | {}  \|  `.'  || {_  
| || .--' \      /|  .'.  || |\  |\  {_ }   | .-. \| {__ /  /\  \|     /| |\ /| || {__ 
`-'`-'     `----' `-'   `-'`-' `-' `---'    `-' `-'`----'`-'  `-'`----' `-' ` `-'`----'
----------------------------------------------------------------------------------------

Author:		Sebastian Nyström
Email:		abeansits@gmail.com
Date:		6/3/2013


---------------
| DESCRIPTION |
---------------

* iPowng is a remake on iOS of the classic game Pong (http://en.wikipedia.org/wiki/Pong).
* It was built on top of UIKit, no game frameworks was hurt in the process. This was intentional since the YouBet application will also be built using the same framework.
* The GUI is generated purely from standard elements and will therefore look it's best even on a Retina Display.

* Build target:     iOS 4.3 
                    (Since the coming YouBet application will need to work on as many devices as possible.)

* Version control:  git 
                    (A local repo holds the history of the project.)

* ARC:              Yes, for the whole project.

* Other:            Enables Hosey warnings.
                    Enabled static analyzer. 
                    TODO & FIXME are treated as warnings.
                    All warnings are treated as errors using the release schemes.
                    (This was done using the amazing liftoff gem: https://github.com/thoughtbot/liftoff)

* Built with:       Xcode v. 4.6

* Tests:            OCUnit test target.
                    (Robustness will be one of the most important buzzwords for the coming YouBet application.)


--------
| CODE |
--------
* KEGameBoardViewController is the main view controller responsible for drawing the actual gameboard. It also handles user input and talks directly to PongCore. 
* KEPongCore is where most of the game logics is abstracted away. It contains params for tweaking the game and talks to KEGameBoardViewController through the KEPongCoreDelegate protocol. This class also acts like the main physics loop in the game with the help of a timer.
* KESettingsViewController is a very simple view controller that pops up when the game is paused. It is also here that the player can select which kind of game they want to play; single or multiplayer.

-------------
| TESTED ON |
-------------

* iPhone Simulator: 3.5" & 4" (iOS 5, 5.1, 6.1)
* iPad Simulator:   (iOS 5, 5.1, 6.1)
* iPhone:           5 (iOS 6.1.1),
* iPad:             3rd Gen (iOS 6.1)


----------------
| KNOWN ISSUES |
----------------
* Running the code in the simulator on iOS5 causes some nasty prints, apparently this is a known issue w a radar:
  http://stackoverflow.com/questions/7961840/what-does-this-gdb-output-mean
  Tried the workarounds without success, moving on since it's only in the simulator.
* Sometimes the ball gets stuck inside a paddle if it enters from the side causing it to dance a bit. 
  This is most likely because of the CGRectIntersect method firing multiple times.


------------
| ROAD MAP |
------------
* Finish TODO's in code!
* Use the accelerometer to control the paddle for player1.
* Connect tweakable game params in PongCore to the settings view (e.g. scoreToWin, difficulty setting).
* The game needs a pretty icon and splash.
* Allow landscape, at least for the iPad. 
* Write Unit tests.
* Ad some GUI that allows pausing of the game.


Happy gaming and peace in the middle east! <3
