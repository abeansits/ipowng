//
//  KEViewController.h
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KEPongCoreDelegate.h"

@interface KEGameBoardViewController : UIViewController <KEPongCoreDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *player1IndicatorLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *player2IndicatorLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *winnerIndicatorLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *player1ScoreLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *player2ScoreLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *player1Padel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *player2Padel;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *startButton;
- (IBAction)tapToStartAction;

- (IBAction)player1LeftTouch;
- (IBAction)player1RightTouch;
- (IBAction)player1Release;

- (IBAction)player2LeftTouch;
- (IBAction)player2RightTouch;
- (IBAction)player2Release;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *ball;

@end
