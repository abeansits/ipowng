//
//  KEGameDelegate.m
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import "KEPongCore.h"

@interface KEPongCore() {
    NSTimer* _gameHeartBeat;
}
@end

@implementation KEPongCore

#pragma mark - Singleton

+ (id)sharedPongCore {
    static dispatch_once_t pred;
    static KEPongCore *sharedPongCore = nil;
    
    dispatch_once(&pred, ^{
        sharedPongCore = [[self alloc] init];
        
        // Tweak these params to change the feeling of the game.
        sharedPongCore.currentGameState         =           KEGameStatePaused;
        sharedPongCore.scoreToWinMatch          =           10;
        sharedPongCore.player1PaddleVelocity    =           15.0f;
        sharedPongCore.player2PaddleVelocity    =           15.0f;
        sharedPongCore.cpuPaddleVelocity        =           7.0f;
        sharedPongCore.ballVelocity             =           CGPointMake(8.0f, 12.0f);
    });
    return sharedPongCore;
}


#pragma mark - Game Logics

// Calling this method will cause a paused game to resume and a new game to start.
- (void)startNewGame {
    [self resetGame];
    [self setCurrentGameState:KEGameStatePaused];
}

- (void)resetGame {
    [self invalidateTimer];
    
    _player1Score = 0;
    _player2Score = 0;
    
    [_delegate resetGame];
}

- (void)setCurrentGameState:(KEGameState)currentGameState {
    _currentGameState = currentGameState;
    
    switch (currentGameState) {
        case KEGameStateRunning:
            [self createAndStartTimer];
            break;
        case KEGameStatePaused:
        case KEGameStateFinished:
        default:
            [self invalidateTimer];
            break;
    }
}

// Call on the game delegate to trigger a 'heartbeat' in the game. This will cause a refresh of the GUI components,
// sort of like a physics loop in a real game engine. =)
- (void)createAndStartTimer {
    if (_delegate && [_delegate respondsToSelector:@selector(gameHeartBeat)])
        _gameHeartBeat = [NSTimer scheduledTimerWithTimeInterval:0.04f target:_delegate selector:@selector(gameHeartBeat) userInfo:nil repeats:YES];
}
             
- (void)invalidateTimer {
    if (_gameHeartBeat) {
        [_gameHeartBeat invalidate];
        _gameHeartBeat = nil;
    }
}


#pragma mark - Score

- (void)setPlayer1Score:(NSInteger)player1Score {
    _player1Score = player1Score;
    _player1IsBallWinner = YES;
    
    if(_player1Score >= _scoreToWinMatch) {
        [self setCurrentGameState:KEGameStateFinished];
        [_delegate player1Wins];
    }
}

- (void)setPlayer2Score:(NSInteger)player2Score {
    _player2Score = player2Score;
    _player1IsBallWinner = NO;
    
    if(_player2Score >= _scoreToWinMatch) {
        [self setCurrentGameState:KEGameStateFinished];
        [_delegate player2Wins];
    }
}


#pragma mark - Updating Ball Velocity

- (void)reverseBallHorizontalVelocity {
    _ballVelocity.x = -(_ballVelocity.x);
}

- (void)reverseBallVerticalVelocity {
    _ballVelocity.y = -(_ballVelocity.y);
}


@end
