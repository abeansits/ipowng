//
//  UILabel+Extension.m
//  iPowng
//
//  Created by Sebastian Nystrom on 11/6/11.
//  Copyright (c) 2011 Konscia. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

- (void)setText:(NSString *)text withBlinkingAnimation:(BOOL)blinking {
    [self setText:text];
    
    if (!blinking)
        return;
    
    [UIView animateWithDuration:0.5f delay:0.0f options:
     UIViewAnimationOptionRepeat
                     animations:^{
                         [UIView setAnimationRepeatCount:4.0f];
                         [self setAlpha:1.0f];
    } 
                     completion:^(BOOL finished){
                         [self setAlpha:0.0f];
    }];
}

@end
