//
//  KEAppDelegate.h
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KEGameBoardViewController;

@interface KEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KEGameBoardViewController *viewController;

@end
