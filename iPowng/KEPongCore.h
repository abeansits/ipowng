//
//  KEGameDelegate.h
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    KEGameStatePaused,
    KEGameStateRunning,
    KEGameStateFinished
} KEGameState;

typedef enum {
    KEGameModeSinglePlayer  =   0,
    KEGameModeMultiPlayer   =   1
} KEGameMode;

@interface KEPongCore : NSObject

@property (nonatomic, unsafe_unretained) NSObject <KEPongCoreDelegate>*     delegate;

@property (nonatomic, assign) NSInteger                                     player1Score;
@property (nonatomic, assign) NSInteger                                     player2Score;
@property (nonatomic, assign) BOOL                                          player1IsBallWinner;    // Ball winner? thihi

// Setting the state of game will start and pause the main game loop.
@property (nonatomic, assign) KEGameState                                   currentGameState;
@property (nonatomic, assign) KEGameMode                                    currentGameMode;

// Tweak the game by setting the following properties.
@property (nonatomic, assign) NSInteger                                     scoreToWinMatch;
@property (nonatomic, assign) CGFloat                                       player1PaddleVelocity;
@property (nonatomic, assign) CGFloat                                       player2PaddleVelocity;
@property (nonatomic, assign) CGFloat                                       cpuPaddleVelocity;
@property (nonatomic, assign) CGPoint                                       ballVelocity;

+ (id)sharedPongCore;

- (void)reverseBallHorizontalVelocity;
- (void)reverseBallVerticalVelocity;

// Controlls the game.
- (void)startNewGame;

@end
