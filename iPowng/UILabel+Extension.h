//
//  UILabel+Extension.h
//  iPowng
//
//  Created by Sebastian Nystrom on 11/6/11.
//  Copyright (c) 2011 Konscia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

// This category will cause the UILabel to blink whenever it's text has been updated.
- (void)setText:(NSString *)text withBlinkingAnimation:(BOOL)blinking;

@end
