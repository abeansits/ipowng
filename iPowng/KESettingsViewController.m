//
//  KESettingsViewController.m
//  iPowng
//
//  Created by Sebastian Nyström on 3/8/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import "KESettingsViewController.h"
#import "KEPongCore.h"

@interface KESettingsViewController() {
    KEPongCore* pongCore;
}
@end

@implementation KESettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        pongCore = [KEPongCore sharedPongCore];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setGameModeSegment:nil];
    [self setStartGameButton:nil];
    [super viewDidUnload];
}

- (IBAction)gameModeChanged {
    [pongCore setCurrentGameMode:_gameModeSegment.selectedSegmentIndex];
}

#pragma mark - Actions

- (IBAction)startGameAction {
    NSLog(@"startGameAction");
    [self.presentingViewController dismissModalViewControllerAnimated:YES];
    [pongCore startNewGame];
}

@end
