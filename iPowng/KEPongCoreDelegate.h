//
//  KEPongCoreDelegate.h
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KEPongCoreDelegate <NSObject>

- (void)gameHeartBeat;

- (void)player1Wins;
- (void)player2Wins;

- (void)resetGame;

@end
