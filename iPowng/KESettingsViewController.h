//
//  KESettingsViewController.h
//  iPowng
//
//  Created by Sebastian Nyström on 3/8/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KESettingsViewController : UIViewController

@property (unsafe_unretained, nonatomic) IBOutlet UISegmentedControl *gameModeSegment;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *startGameButton;

- (IBAction)startGameAction;

@end
