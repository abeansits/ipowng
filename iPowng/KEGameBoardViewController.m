//
//  KEViewController.m
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import "KEGameBoardViewController.h"
#import "KEPongCore.h"
#import "UILabel+Extension.h"
#import "KESettingsViewController.h"

#import <AVFoundation/AVFoundation.h>


@interface KEGameBoardViewController() {
    KEPongCore* pongCore;
    
    // Paddle controls
    // I would rather have had a direction vector class, but there is no time to write that
    // so a simple integer will have to do. A positive value indicates that the paddle should
    // travel along the x-axis on a positive direction. While a negative value indicates
    // the opposite.
    NSInteger _player1PaddleDirection;
    NSInteger _player2PaddleDirection;
    
    // For playing simple game sounds.
    AVAudioPlayer* _audioPlayer;
    
    KESettingsViewController* _settingViewController;
    
    BOOL _firstStart;
}
@end


@implementation KEGameBoardViewController

#pragma mark - View Controller Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        pongCore = [KEPongCore sharedPongCore];
        [pongCore setDelegate:self];
                
        // Sound was retrieved from: http://www.findsounds.com and converted using afconvert.
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"pong" ofType:@"caf"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
        
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
        [_audioPlayer prepareToPlay];
        
        _firstStart = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if(!_settingViewController) {
        _settingViewController = [[KESettingsViewController alloc] initWithNibName:nil bundle:nil];
        
        // Only relevant for iPad
        _settingViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setPlayer1IndicatorLabel:nil];
    [self setPlayer2IndicatorLabel:nil];
    [self setPlayer1Padel:nil];
    [self setPlayer2Padel:nil];
    [self setBall:nil];
    [self setPlayer1ScoreLabel:nil];
    [self setPlayer2ScoreLabel:nil];
    [self setWinnerIndicatorLabel:nil];
    [self setStartButton:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated {
    if(_firstStart) {
        [self animateInSettingsViewController];
        _firstStart = NO;
    }
}


#pragma mark - GUI

- (void)animateInSettingsViewController {
    [self presentModalViewController:_settingViewController animated:YES];
}

- (void)updateScore {
    [_player1ScoreLabel setText:[NSString stringWithFormat:@"%d", pongCore.player1Score] withBlinkingAnimation:YES];
    [_player2ScoreLabel setText:[NSString stringWithFormat:@"%d", pongCore.player2Score] withBlinkingAnimation:YES];
}

// TODO: some refactoring needed below
- (void)movePaddles {
    CGFloat paddleVelocity = pongCore.player1PaddleVelocity * _player1PaddleDirection;
    CGFloat newPaddlePosition = _player1Padel.center.x + paddleVelocity;
    
    // A paddle should not be able to move outside the screen.
    if(newPaddlePosition > 0 && newPaddlePosition < self.view.frame.size.width)
        [_player1Padel setCenter:CGPointMake(newPaddlePosition, _player1Padel.center.y)];
    
    if(pongCore.currentGameMode == KEGameModeMultiPlayer) {
        // Human controll of the other paddle.
        paddleVelocity = pongCore.player2PaddleVelocity * _player2PaddleDirection;
        newPaddlePosition = _player2Padel.center.x + paddleVelocity;
        
        if(newPaddlePosition > 0 && newPaddlePosition < self.view.frame.size.width)
            [_player2Padel setCenter:CGPointMake(newPaddlePosition, _player2Padel.center.y)];
    } else {
        // So called AI. ;)
        [self moveCPUPaddle];
    }
}

// The CPU paddle (player2Paddle) will move in the direction of the ball according to the specified velocity.
- (void)moveCPUPaddle {
    if (_ball.center.x > _player2Padel.center.x)
        [_player2Padel setCenter:CGPointMake(_player2Padel.center.x + pongCore.cpuPaddleVelocity, _player2Padel.center.y)];
    else if (_ball.center.x < _player2Padel.center.x)
        [_player2Padel setCenter:CGPointMake(_player2Padel.center.x - pongCore.cpuPaddleVelocity, _player2Padel.center.y)];
}

- (void)resetGUI {
    [_player1Padel setCenter:CGPointMake(self.view.center.x, _player1Padel.center.y)];
    [_player2Padel setCenter:CGPointMake(self.view.center.x, _player2Padel.center.y)];
    [_ball setCenter:self.view.center];
    [_startButton setHidden:NO];
}

- (void)resetServe {
    // This is needed since there is no way of being accurate about invalidating the timer.
    if(pongCore.currentGameState == KEGameStateFinished)
        return;
    
    // Pause the game and the so called 'heart beat'.
    [pongCore setCurrentGameState:KEGameStatePaused];
    
    // Reset the GUI between each serve.
    [self resetGUI];
    
    // When a player scores he should also make the serve,
    // which means that the ball will move in the direction of the opponent.
    if (pongCore.player1IsBallWinner && pongCore.ballVelocity.y > 0.0f)
        [pongCore reverseBallVerticalVelocity];
    else if (!pongCore.player1IsBallWinner && pongCore.ballVelocity.y < 0.0f)
        [pongCore reverseBallVerticalVelocity];
}


#pragma mark - Actions

- (IBAction)tapToStartAction {
    if (pongCore.currentGameState == KEGameStatePaused) {
        [_startButton setHidden:YES];
        [_player1IndicatorLabel setHidden:YES];
        [_player2IndicatorLabel setHidden:YES];
        [pongCore setCurrentGameState:KEGameStateRunning];
    }
}

- (IBAction)player1LeftTouch {
    _player1PaddleDirection = -1;
}

- (IBAction)player1RightTouch {
    _player1PaddleDirection = 1;
}

- (IBAction)player1Release {
    _player1PaddleDirection = 0;
}

- (IBAction)player2LeftTouch {
    _player2PaddleDirection = -1;
}

- (IBAction)player2RightTouch {
    _player2PaddleDirection = 1;
}

- (IBAction)player2Release {
    _player2PaddleDirection = 0;
}


#pragma mark - KEPongCoreDelegate

- (void)gameHeartBeat {
//    NSLog(@"*beat* *beat*");
    
    // MOVE BALL //
    // Moving the ball according to the velocity specified in ballVelocity.
    CGPoint newBallCenter = CGPointMake(_ball.center.x + pongCore.ballVelocity.x, _ball.center.y + pongCore.ballVelocity.y);
    [_ball setCenter:newBallCenter];

    // BOUNCE OF THE WALLS //
    // When the ball 'hits' an edge or a paddle the velocity is reversed causing a 'bounce' to take place.
    if ( (_ball.frame.origin.x + _ball.frame.size.width) >= self.view.frame.size.width || _ball.frame.origin.x <= 0 ) {
        [pongCore reverseBallHorizontalVelocity];
        [_audioPlayer play];
    }
    
    // BOUNCE OF THE PADDLES //
    // Another round of checks using UIKit to detect a collison between the ball and the paddle.
    if (CGRectIntersectsRect(_ball.frame, _player1Padel.frame) || CGRectIntersectsRect(_ball.frame, _player2Padel.frame)) {
        [pongCore reverseBallVerticalVelocity];
        [_audioPlayer play];
    }
    
    // SCORE //
    // When the ball hits the area behind one of the paddles, the opposite player should increase her score.
    if (_ball.frame.origin.y <= 0.0f) {
        // Player 1 has scored!
        pongCore.player1Score++;
        [self resetServe];
        [self updateScore];
    } else if ( (_ball.frame.origin.y + _ball.frame.size.height) >= self.view.frame.size.height) {
        // Player 2 has scored!
        pongCore.player2Score++;
        [self resetServe];
        [self updateScore];
    }
    
    // PADDLE MOVEMENT //
    [self movePaddles];
}

// A new game indicates that we should reset the GUI according to the game mode.
- (void)resetGame {
    [_player2IndicatorLabel setText:(pongCore.currentGameMode == KEGameModeSinglePlayer ? @"CPU" : @"Player 2")];
    [_player1IndicatorLabel setHidden:NO];
    [_player2IndicatorLabel setHidden:NO];

    [self resetGUI];
}

- (void)player1Wins {
    [_winnerIndicatorLabel setText:@"Player1 is WIN!" withBlinkingAnimation:YES];
    [self finishGame];
}

- (void)player2Wins {
    [_winnerIndicatorLabel setText:(pongCore.currentGameMode == KEGameModeSinglePlayer ? @"CPU is WIN!" : @"Player2 is WIN!") withBlinkingAnimation:YES];
    [self finishGame];
}

- (void)finishGame {
    [_startButton setHidden:YES];
    [_winnerIndicatorLabel setAlpha:1.0];
    [self performSelector:@selector(animateInSettingsViewController) withObject:nil afterDelay:3.0f];
}

@end
