//
//  KEAppDelegate.m
//  iPowng
//
//  Created by Sebastian Nyström on 3/6/13.
//  Copyright (c) 2013 Konscia. All rights reserved.
//

#import "KEAppDelegate.h"

#import "KEGameBoardViewController.h"

// TODO: All TODO's in this project are treated as a warning. =)

@implementation KEAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[KEGameBoardViewController alloc] initWithNibName:@"KEGameBoardViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[KEGameBoardViewController alloc] initWithNibName:@"KEGameBoardViewController_iPad" bundle:nil];
    }
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
